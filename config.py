import json


class Config():

    def __init__(self, config_file):
        self.config_object = json.load(config_file)

    def config_for_deployment(self, deployment='dev'):
        config = self.config_object['deployment'][deployment]
        return config
