__author__ = 'steveph'

import psycopg2
from psycopg2.extras import LoggingConnection
class Database():

    def __init__(self, logger, host="localhost", port=5432, user=None, password=None, database=None, autocommit=True):

        self.connection = psycopg2.connect(
            connection_factory=LoggingConnection,
            host=host, port=port,
            user=user, password=password, database=database
        )
        self.connection.initialize(logger)
        if autocommit:
            self.connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

    def get_connection(self):
        return self.connection
