import sys
import argparse
from config import Config
from db import Database
import logging
import psycopg2.extensions

STAGING_SCHEMA = 'staging'


class Loader():

    def __init__(self, input_file, db_connection, date, distributor):

        self.input_file = input_file
        self.db_connection = db_connection
        self.date = date
        self.distributor = distributor
        self.staging_table_name = "product_temp_{}".format(date)
        self.logger = logging.getLogger()

    def run(self):

        self.load_data()
        self.transform_data()

    def load_data(self):

        cursor = self.db_connection.cursor()

        sql = """
            CREATE schema IF NOT EXISTS {schema}
        """.format(schema=STAGING_SCHEMA)
        cursor.execute(sql)

        sql = """
            DROP TABLE IF EXISTS {schema}.{table_name};
            CREATE TABLE {schema}.{table_name} (data jsonb)
        """.format(schema=STAGING_SCHEMA, table_name=self.staging_table_name)
        self.logger.info("Creating staging table")
        cursor.execute(sql)

        self.logger.info("Copying data into staging table")
        cursor.copy_expert("""
            COPY {schema}.{table_name} FROM STDIN CSV
            QUOTE e'\x01'
            DELIMITER e'\x02'
        """.format(schema=STAGING_SCHEMA, table_name=self.staging_table_name), self.input_file)
        cursor.close()

    def transform_data(self):

        insert_condition = {'date': self.date, 'distributor_name': self.distributor}
        cursor = self.db_connection.cursor()

        #Insert distributor if it's not existed
        sql = """
            INSERT INTO distributors
                (name)
            SELECT
                %(distributor_name)s
            WHERE
                NOT EXISTS (
                    SELECT
                        name
                    FROM
                       distributors
                    WHERE name = %(distributor_name)s
                )
        """
        self.logger.info("Loading distributor data")
        cursor.execute(sql, {'distributor_name': self.distributor})



        #Insert new product
        sql = """
            with product_temp as (
                SELECT
                    d.id as distributor_id, data->>'name' as name,
                    data->>'model' as model, data->>'brand' as brand, data->>'url' as url,
                    data->>'image' as image, data->>'description' as description,
                    to_tsvector(
                        'english',
                        CAST(data->>'name' as TEXT) || ' ' ||
                        CAST(data->>'brand' as TEXT) || ' ' ||
                        CAST(data->>'model' as TEXT)
                    ) as text_search,
                    row_number() over
                        (PARTITION BY d.id, data->>'model', data->>'brand', data->>'name' ORDER BY data->>'name') as rownum
                FROM
                    {schema}.{table_name} s
                    LEFT JOIN products p
                        ON (s.data->>'url' = p.url OR s.data->>'model' = p.model) AND s.data->>'name' = p.name
                    LEFT JOIN distributors d
                        ON d.name = %(distributor_name)s
                WHERE
                    p.id IS NULL AND p.distributor_id IS NULL
                    AND (data->>'model')::TEXT <> 'null' AND (data->>'name')::TEXT <> 'null'
            )

            INSERT INTO products
                (distributor_id, name, model, brand, url, image, description, text_search)
            SElECT
                distributor_id, name, model, UPPER(brand), url, image, description, text_search
            FROM
                product_temp
            WHERE
                rownum=1
        """.format(schema=STAGING_SCHEMA, table_name=self.staging_table_name )
        self.logger.info("Loading product")
        cursor.execute(sql, {'distributor_name' : self.distributor})

        #DELETE existing product for given (distributor, date) and load new one in the following step
        sql = """
            DELETE
            FROM
                product_prices pp
            USING products p
            WHERE
                pp.product_id = p.id AND
                distributor_id in (SELECT id FROM distributors WHERE name=%(distributor_name)s)
                AND date=%(date)s::DATE
        """
        self.logger.info("Deleting existing prices for {distributor_name} on {date}".format(**insert_condition))
        cursor.execute(sql, insert_condition)

        #load new product price for given date and distributor
        sql = """
            with product_price_temp as (
                SELECT
                    p.id as product_id,
                    %(date)s::DATE as date,
                    CAST(s.data->>'price' as float) as price,
                    row_number() over (PARTITION BY p.id ORDER BY s.data->>'price') as rownum
                FROM
                    products p
                    JOIN {schema}.{table_name} s
                        ON (s.data->>'url' = p.url OR s.data->>'model' = p.model)  AND s.data->>'name' = p.name
                    JOIN distributors d
                        ON p.distributor_id = d.id and d.name = %(distributor_name)s
            )

            INSERT INTO product_prices
                (product_id, date, price)
            SELECT
                product_id, date, price
            FROM
                product_price_temp
            WHERE
                rownum = 1
        """.format(schema=STAGING_SCHEMA, table_name=self.staging_table_name, distributor_name = self.distributor)
        self.logger.info("Inserting product prices")
        cursor.execute(sql, insert_condition)
        self.db_connection.commit()
        self.db_connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        cursor.execute("ANALYZE")

        sql = """
            DROP TABLE {schema}.{table_name}
        """.format(schema=STAGING_SCHEMA, table_name=self.staging_table_name)
        self.logger.info("Dropping staging table")
        cursor.execute(sql)

        sql = """
            CREATE table IF NOT EXISTS available_dates (date DATE);
            INSERT INTO available_dates
            SELECT %(date)s
            FROM available_dates
            WHERE NOT EXISTS (SELECT 1 FROM available_dates where date=%(date)s)
            LIMIT 1
        """
        cursor.execute(sql, {'date': self.date})

        sql = """
            DELETE FROM cache
            USING distributors
            WHERE
                name=%(distributor_name)s AND distributors @> ARRAY[distributors.id::TEXT] OR distributors = ARRAY[]::TEXT[]
        """
        self.logger.info("Deleting cache")
        cursor.execute(sql, {'distributor_name': self.distributor})
        
        cursor.close()


def console_ui(arguments):

    parser = argparse.ArgumentParser(description='loading json file into staging database')

    parser.add_argument('-d', '--date', required=True, dest='date',
                        help="Date of the crawled products")

    parser.add_argument('-i', '--input', required=True, dest='input', type=argparse.FileType("r"),
                        help="Path to json file")

    parser.add_argument('--config-file', dest='config_file', required=True, type=argparse.FileType("r"),
                        help="Database json config file")

    parser.add_argument('--deployment', dest='deployment', default='dev', choices=["dev","prod"],
                        help="Deployment environment")

    parser.add_argument('--distributor', dest='distributor', required=True,
                        help="name of the distributor")

    parser.add_argument('-v', '--verbose', dest='verbose', action='count', help="Increase verbosity")

    args = parser.parse_args(arguments)

    if args.verbose >= 3:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    logger = logging.getLogger()

    config = Config(args.config_file)

    db_connection = Database(logger, **config.config_for_deployment(args.deployment)).get_connection()

    Loader(args.input, db_connection, args.date, args.distributor).run()

    db_connection.close()

if __name__ == '__main__':
    console_ui(sys.argv[1:])
